const express               =   require('express');
const dotenv                =   require('dotenv')
const mongoose              =   require("mongoose");
const cors                  =   require("cors");
const usersRouter           =   require("./app/routes/users.routes");
const reportsRouter         =   require("./app/routes/reports.routes");

dotenv.config();

// Connect to DB

const mongoDB = process.env.MONGODB_ULR_PRODUCTION;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Connected to Database successfully:"));


const server = express();

server.use(cors());
server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use('/users', usersRouter);
server.use('/reports', reportsRouter)



server.listen(8080, () => console.log('Server Started'));





