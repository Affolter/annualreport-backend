const mongoose = require('mongoose');


/**
 * Promisified Mongoose model for user document.
 * That means, you need to call `findByIdAsync` instead of `findById`
 * to use a function that returns a promise.
 *
 * @class User
 * @property {string}           email                              - username as user email
 * @property {string}           password                           - hashed password
 * @property {string}           role                               - user role
 * @property {date}             created                            - date of creation

 */

const User = mongoose.model(
    'User',
    new mongoose.Schema(
        {
            email: {
                type: String,
                unique: true,
                lowercase: true,
                trim: true,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            role: {
                type: String,
                enum: ['ADMIN', 'MAINTAINER', 'REPORTER', 'GUEST'],
                default: 'REPORTER'
            },
            created: {
                type: Date,
                default: Date.now
            }
        },
        {collection: "users"}
    )
);

module.exports = User;


