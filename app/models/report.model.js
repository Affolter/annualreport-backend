const mongoose = require('mongoose');

const Report = mongoose.model(
    'Report',
    new mongoose.Schema(
        {
            year: {
                type: String,
                unique: true,
                lowercase: true,
                trim: true,
                required: true
            },
            data: {
                type: Object
            }
        },
        {collection: "reports"}
    )
);

module.exports = Report;
