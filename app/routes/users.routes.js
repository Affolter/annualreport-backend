const express   = require('express');
const ctrl      = require('../controllers/user.controller');
const valid     = require('../controllers/validate.controller');
const auth      = require("../controllers/auth.controller");
const access    = require('../controllers/access.controller')
const {authenticateToken} = require("../controllers/auth.controller");
const router    = express.Router();


const validatePostReq = (req, res, next) => {
    try {
        const {email, password} = req.body;
        [
            valid.isDefined(email),
            valid.isEmail(email),
            valid.isDefined(password),
            valid.minStringLength(password, process.env.PASSWORD_MIN_LENGTH)
        ].every(x => x)
            ? next()
            : res.status(400).send({message: `Invalid Input Email: ${email} Password: ${password}`});
    }
    catch (error) {
        res.status(500).send({message: error});
    }
}

router.get("/", auth.authenticateToken, access.isAdmin, ctrl.getUsers, (req, res) => {
    res.json(res.locals.users)
})

router.get('/user', authenticateToken, access.isReporter, ctrl.getUser, (req, res) => {
    res.json(res.locals.user);

});

router.post('/signIn', validatePostReq, ctrl.sign_in, (req, res) => {
    res.json({accessToken: res.locals.accessToken});
    console.log({message: 'Login Sucessfull', who: req.body.email});
});


router.post('/register', validatePostReq, ctrl.createUser, (req, res) => {
    res.send(`${req.body.email} successfully created`);
});

module.exports = router;
