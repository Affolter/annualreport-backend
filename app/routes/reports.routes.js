const express = require('express');
const ctrl    = require("../controllers/report.controller");
const auth    = require('../controllers/auth.controller');
const access  = require('../controllers/access.controller');
const {isMaintainer} = require("../controllers/access.controller");


const router = express.Router();


router.get("/init", ctrl.initFirstYear, (req, res) => {
    res.send({message: res.locals.init})
});

// Retrieve all years
router.get("/", auth.authenticateToken, access.isReporter, ctrl.getReports, (req, res) => {
 res.json(res.locals.reports);
});


// Retrieve a single year with id
router.get("/:year", ctrl.getReport,(req, res) => {
    res.json(res.locals.report);
});

// Create a new entry
router.post("/", auth.authenticateToken, isMaintainer, ctrl.createReport, (req, res) => {
    res.json(res.locals.newreport);
});


// Update a year with id
router.put("/:id", auth.authenticateToken, isMaintainer, ctrl.updateReport,(req, res) => {
    res.json(res.locals.updatedReport);
});

// Delete a year with id
router.delete("/:id", auth.authenticateToken, isMaintainer, ctrl.removeReport, (req, res) => {
    res.status(200).send({message: 'Successfully delete'});
});

module.exports = router;
