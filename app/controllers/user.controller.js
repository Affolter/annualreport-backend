const dotenv = require('dotenv');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const auth = require('../controllers/auth.controller');
const userModel = require('../models/user.model')

dotenv.config();


const User = mongoose.model('User');


/**
 * Description of findUser.
 * @function
 * @param {Object} req           - Express request object
 * @param {Object} res           - Express response object
 * @param {Object} projection      -
 * @return {User}
 */

const findUser = async ({id, email}, res, projection = {}) => {
    try {
        return await User.findOne({
            $or: [
                {'_id': `${mongoose.Types.ObjectId(id)}`}, {'email': email}
            ]
        }, projection);
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
}


const sign_in = async (req, res, next) => {
    const {id, email} = req.body
    try {
        const user = await findUser({id: id, email: email}, res, {});

        if (user == null || await (!bcrypt.compare(req.body.password, user.password))) {
            return res.status(400).send({message: 'Username or Password invalid'});
        }

        res.locals.accessToken = await auth.genAccessToken(user.email, user.role);
        res.locals.refreshToken = await auth.genRefreshToken(user.email, user.role);

    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const getUsers = async (req, res, next) => {
    try {
        res.locals.users = await User.find();
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const getUser = async (req, res, next) => {
    const {id, email} = res.locals.decoded;
    try {
        res.locals.user= await findUser({id: id, email: email}, res, {password: 0, created: 0});
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const createUser = async (req, res, next) => {
    try {
        await new User({
            email: req.body.email,
            password: await bcrypt.hash(req.body.password, 10)
        }).save();
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const updatePassword = async (req, res, next) => {
    try {
        const user = findUser(req, res);

        if (user == null || (!bcrypt.compare(req.body.password, user.password))) {
            return res.status(400).send({message: 'Username or Password invalid'});
        }

        if (bcrypt.compare(req.body.newPassword, user.password)) {
            return res.status(400).send({message: 'New Password is invalid'});
        }

        User.updateOne(
            {id: user._id},
            {$set: {password: await bcrypt.hash(req.body.newPassword, 10)}}
        );

    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const deleteUser = async (req, res, next) => {
    try {

        await User.findOneAndDelete({id: req.body.user._id})
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


module.exports = {
    getUsers,
    getUser,
    createUser,
    deleteUser,
    sign_in,
    updatePassword,
}
