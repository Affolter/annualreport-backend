
const isAdmin = (req, res, next) => {
    try {
        res.locals.decoded.role === 'ADMIN'
        ? next()
        : res.status(403).send({message: 'Admin rights needed'})
    } catch (error) {
        res.status(400).send({message: 'Invalid input ' + error})
    }
}


const isMaintainer = (req, res, next) => {
    try {
        res.locals.decoded.role === 'ADMIN' ||   res.locals.decoded.role === 'MAINTAINER'
         ? next()
         : res.status(403).send({message: 'Maintainer rights needed'})
    }catch (error) {
        res.status(400).send({message: 'Invalid input ' + error})
    }
}

const isReporter = (req, res, next) => {
    try {
        res.locals.decoded.role === 'ADMIN' || res.locals.decoded.role === 'MAINTAINER' || res.locals.decoded.role === 'REPORTER'
            ? next()
            : res.status(403).send({message: 'Reporter rights needed'})
    }catch (error) {
        return res.status(400).send({message: 'Invalid input ' + error})
    }
}

module.exports = {
    isAdmin,
    isMaintainer,
    isReporter

}
