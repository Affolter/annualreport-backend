const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const authenticateToken = async (req, res, next) => {

    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1]

    if(authHeader == null || token == null) return res.status(401).send({message: 'Token is invalid'});

    try {
        res.locals.decoded = verifyToken(token);
    }
    catch (error) {
        return res.status(403).send({message: error});
    }
    next();
};


const verifyToken = token => {
    return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, null, null);
}

const genAccessToken = (email, role) => {
    return jwt.sign({email: email, role: role}, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '2h'}, null);
}

const genRefreshToken = (email, role) => {
    return jwt.sign({email: email, role: role}, process.env.REFRESH_TOKEN_SECRET, null, null);
}


module.exports = {
    authenticateToken,
    verifyToken,
    genAccessToken,
    genRefreshToken,
}
