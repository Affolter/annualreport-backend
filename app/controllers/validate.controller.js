
const isDefined = value => !(value == null);
const notEmptyString = string => string?.trim();
const isEmail = (email) => email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
const minStringLength = (string, length) => string.length >= length;


module.exports = {
    isDefined,
    notEmptyString,
    isEmail,
    minStringLength
};
