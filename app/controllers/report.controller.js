const dotenv = require('dotenv');
const mongoose = require('mongoose');
const initYear = require('../config/initData')
const reportModel = require('../models/report.model')

dotenv.config();
const Report = mongoose.model('Report');

const findOne = async ({id, year}, res) => {
    try {
        return await Report.findOne({
            $or: [
                {'_id': `${mongoose.Types.ObjectId(id)}`}, {'year': year}
            ]
        });
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
}

const getReport = async (req, res, next) => {
    const {id, year} = req.params;
    try{
        res.locals.report = await findOne({id: id, year: year}, res);
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();

}

const getReports = async (req, res, next) => {
    try {
        res.locals.reports = await Report.find()
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}

const createReport = async (req, res, next) => {
    try {
        res.locals.newreport = await new Report({year: req.body.year, data: req.body.data}).save();
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}

const updateReport = async (req, res, next) => {
    const query = {}
    const id = req.body._id;
    query["year"] = req.body.year
    query["data"] = req.body.data
    try {
        res.locals.updatedReport = await Report.findByIdAndUpdate(id, query , {useFindAndModify: false })
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const removeReport = async (req, res, next) => {
    try {
        res.locals.removedReport = await Report.findOneAndRemove({year: req.params.id});
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}


const initFirstYear = async (req, res, next) => {
    try {
        res.locals.init = await new Report({year: initYear.year.year, data: initYear.year.data}).save();
    } catch (error) {
        return res.status(500).send({
            message: error.message
        });
    }
    next();
}

module.exports = {
    getReports,
    getReport,
    createReport,
    updateReport,
    removeReport,
    initFirstYear,
}
